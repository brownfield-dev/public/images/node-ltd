FROM ubuntu:20.10

RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y \
      bash \
      build-essential \
      ca-certificates \
      curl \
      htop \
      locales \
      man \
      python3 \
      python3-pip \
      software-properties-common \
      unzip \
      vim \
      wget && \
    add-apt-repository ppa:git-core/ppa && \
    curl -sL https://deb.nodesource.com/setup_current.x | bash - && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y git nodejs && \
    npm i -g yarn && \
    adduser --gecos '' --disabled-password coder

USER coder

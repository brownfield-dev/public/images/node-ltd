# Developer image with restrictions

In Coder, one of the methods for limiting risk is to include images that have only the right tooling to get the job done. 

Many of Coder's sample images include sudo and allow the `coder` user to install new packages or arbitrary binaries. 

This image includes many packages as you can see from the `Dockerfile` but `sudo` is not among them. It also does not include Docker since the docker socket is a root privilege escalation. 

## Limitations

1. Without sudo, any upgrades to packages or installations of new packages need to run through the pipeline. 
2. No systemctl, docker, or other `cvm` related tools since this environment is non-root

## Build pipeline

This repo has a job that builds the container, includes the gitlab container scanner, and has a release job.

The release job runs when a git tag is added to the repo and takes the `HEAD` commit hash's docker image and re-tags it 3 ways:

1. Full git tag such as `v1.2.3`
2. Major version from git tag `v1`
3. `latest`

Coder sees the `v1` and `latest` tags update and will show which environments are not on the most recent image with that tag.  If an envrionment is created from the major.minor.patch tag, Coder won't try to update it or flag it as outdated. 
